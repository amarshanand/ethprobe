const HDWalletProvider = require("@truffle/hdwallet-provider");
require("dotenv").config();

if (!process.env.ADMIN_PRIVATE_KEY || !process.env.INFURA_PROJECT_ID)
  throw new Error(
    "FATAL: Either the .env file is missing, or it doesnt have ADMIN_PRIVATE_KEY and INFURA_PROJECT_ID fields. Please refer to Readme for the fix"
  );

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 9545,
      network_id: "*",
    },
    goerli: {
      provider: () =>
        new HDWalletProvider(
          process.env.ADMIN_PRIVATE_KEY,
          `https://goerli.infura.io/v3/${process.env.INFURA_PROJECT_ID}`
        ),
      network_id: 5, //Goerli's id
      gas: 5000000, //gas limit
      confirmations: 1, // # of confs to wait between deployments. (default: 0)
      timeoutBlocks: 200, // # of blocks before a deployment times out  (minimum/default: 50)
      skipDryRun: true, // Skip dry run before migrations? (default: false for public nets )
    },
    matic: {
      provider: () =>
        new HDWalletProvider(
          process.env.ADMIN_PRIVATE_KEY,
          `https://matic-mumbai.chainstacklabs.com`
        ),
      network_id: 80001,
      confirmations: 2,
      timeoutBlocks: 200,
      skipDryRun: true,
    },
  },
};
