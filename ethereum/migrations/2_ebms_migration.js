/**
 * This migration script is our 'infrastructure-setup'.
 * It reads the SmartContracts (*.sol files), and deploys them to the network that is mentioned in package.json's deploy script.
 * In EthProbe, we perform the following sequence of deployments:
 * 1. Deploy the Factory SC. This requires us to specify the factory user (which is where all earnings of the Factory goes)
 * 2. Deploy the Marketplace SC.
 * 3. Deploy the User SC, which requires the address of the Factory and the Marketplace SC.
 * 4. Deploy the Accounts SC, which requires the address of the User SC.
 * All this is performed by the Admin user (identified by the ADMIN_PRIVATE_KEY in .env file)
 */

const Factory = artifacts.require('./Factory.sol');
const Marketplace = artifacts.require('./Marketplace.sol');
const User = artifacts.require('./User.sol');
const Accounts = artifacts.require('./Accounts.sol');

const { users } = require('../users');
let factoryUser = 0x0;
Object.entries(users).some(([ hash, { name } ]) => {
    if (name === 'Factory') {
        factoryUser = hash;
        return true;
    }
});

module.exports = function(deployer) {
    deployer.deploy(Factory, factoryUser).then(function() {
        return deployer.deploy(Marketplace).then(function() {
            return deployer.deploy(User, Factory.address, Marketplace.address).then(function() {
                return deployer.deploy(Accounts, User.address);
            });
        });
    });
};
