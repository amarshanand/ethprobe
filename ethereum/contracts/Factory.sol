pragma solidity 0.5.16;

import "./User.sol";

/**
 * Factory is a place where products are created. It charges a flat fee of 10 wei
 * for producing anything.
 */
contract Factory {
    // the owner of the contract
    address owner;

    // the factory User. The Factory contrcat is stateless, hence we cant deposit any ether in it.
    // all ethers passed to this contrcats wil be passed to factoryUser
    address payable factoryUser;

    // a simple sequentional counter of how many products the factory has produced
    uint256 nextProductId;

    // events to notify client UI of an update in the Factory ledger and inventory
    event FactoryLedgerUpdated(
        address user,
        uint256 date,
        string desc,
        uint256 value
    );

    // constructor
    constructor(address payable _factoryUser) public {
        owner = msg.sender;
        factoryUser = _factoryUser;
        nextProductId = 0;
    }

    /**
     * Product creation for the Factory involves:
     * 1. Ensuring that exactly 10 Finney has been provided as fees
     * 2. Emitting Events to log / notify-the-client of the product creation
     * 3. Notifying the calling User Contract of the Product creation
     * 4. Transferring the 10 Finney received for the production to factoryUser
     * Note: 10 Finney = 10000000000000000 wei
     */
    function createProduct(
        string memory _desc,
        uint256 _price,
        address payable _user
    ) public payable {
        require(
            msg.value == 10000000000000000,
            "Exactly 10 Finney are required to create a product"
        );
        uint256 buildTime = now;
        emit FactoryLedgerUpdated(factoryUser, buildTime, _desc, msg.value);
        User(msg.sender).productCreatedCB(
            nextProductId,
            buildTime,
            _desc,
            _price,
            _user
        );
        factoryUser.transfer(msg.value);
        nextProductId++;
    }

    // destructor of the contract
    function kill() public {
        require(
            msg.sender == owner,
            "only the owner of this contract can destroy it"
        );
        selfdestruct(msg.sender);
    }

    // enusre that none transfers anything to the Factory (no undertable ;)
    function() external payable {
        revert();
    }
}
