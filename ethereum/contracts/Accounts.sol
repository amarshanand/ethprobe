pragma solidity 0.5.16;

import "./User.sol";

/**
 * To maintain user accounts, this contract simply emits an Event that records a
 * User's ethereum address against his details
 */

contract Accounts {
    // the owner of the contract
    address owner;

    // deployed instance of a User SmartContract
    address payable user;

    // event to notify of a new account being added
    event AccountAdded(
        address payable _address,
        string name,
        string about,
        string description,
        string image,
        string url
    );

    // constructor
    constructor(address payable _user) public {
        user = _user;
        owner = msg.sender;
    }

    /**
     * Process of creating a user is:
     * 1. Emit an AccountAdded event to log the user details
     * 2. Inform User SmartContract by calling userCreated() so that the txn fees of
     *    Account creation appears on the User's ledger
     */
    function addAccount(
        string memory _name,
        string memory _about,
        string memory _description,
        string memory _image,
        string memory _url
    ) public {
        emit AccountAdded(
            msg.sender,
            _name,
            _about,
            _description,
            _image,
            _url
        );
        User(user).userCreated(msg.sender);
    }

    // destructor of the contract
    function kill() public {
        require(
            msg.sender == owner,
            "only the owner of this contract can destroy it"
        );
        selfdestruct(msg.sender);
    }

    // enusre that none transfers anything to this account
    function() external payable {
        revert("Cannot accept any Ether directly");
    }
}
