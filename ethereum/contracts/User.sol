pragma solidity 0.5.16;

import "./Factory.sol";
import "./Marketplace.sol";

/**
 * User is the core of our system. They can produce and buy a product.
 */
contract User {
    // the owner of the contract
    address owner;

    // deployed instance of a Factory and Marketplace SmartContract
    address payable factory;
    address payable marketplace;

    // event to notify client UI of an update in the User's ledger and Inventory
    event UserLedgerUpdated(
        address user,
        uint256 date,
        string desc,
        int256 value
    );
    event UserInventoryUpdated(
        address user,
        uint256 productId,
        string desc,
        int256 price,
        bool isSelling
    );

    // constructor
    constructor(address payable _factory, address payable _marketplace) public {
        // store the deployed instance of the Factory and the Marketplace
        factory = _factory;
        marketplace = _marketplace;
        owner = msg.sender;
    }

    /**
     * The process of product creation is as follows:
     * 1. The client app sends a request to the UserSC to produce
     * 2. UserSC ensures that exactly 10 Finney has been sent alongwith the request
     * 3. UserSC sends 10 Finney with product desc to Factory SC
     * This is where User SC stops. FactorySC calls productCreatedCB() after the product has been built
     * Note: 10 Finney = 10000000000000000 wei
     */
    function createProduct(uint256 _price, string memory _desc) public payable {
        require(
            msg.value == 10000000000000000,
            "Exactly 10 Finney are required to create a product"
        );
        emit UserLedgerUpdated(msg.sender, now, _desc, -1 * int256(msg.value));
        Factory(factory).createProduct.value(msg.value)(
            _desc,
            _price,
            msg.sender
        );
    }

    /**
     * This method can only be called by the Factory, after the Factory has finished manufacturing the product.
     * The User then updates its inventory and puts the product for sale in the marketplace
     */
    function productCreatedCB(
        uint256 _productId,
        uint256 _buildTime,
        string memory _desc,
        uint256 _price,
        address payable _user
    ) public {
        require(
            msg.sender == factory,
            "this method can be called only by the factory"
        );
        emit UserInventoryUpdated(
            _user,
            _productId,
            _desc,
            int256(_price),
            true
        );
        Marketplace(marketplace).listProduct(
            _productId,
            _price,
            _desc,
            _buildTime,
            _user
        );
    }

    /**
     * This method simply contacts the Marketplace contract to buy a Product for the calling user.
     * Once Marketplace has concluded the purchase, it calls the productBoughtCB().
     */
    function buyProduct(uint256 _productId) public payable {
        Marketplace(marketplace).buyProduct.value(msg.value)(
            _productId,
            msg.sender
        );
    }

    /**
     * This method will be called by the Marketplace after a request to buy a Product has been accepted.
     * It simply emits the ProductBought event to inform the client UI of the purchase.
     */
    function productBoughtCB(
        uint256 _productId,
        address _buyer,
        string memory _desc,
        uint256 _price
    ) public {
        require(
            msg.sender == marketplace,
            "this method can be called only by the marketplace"
        );
        emit UserLedgerUpdated(_buyer, now, _desc, -1 * int256(_price));
        emit UserInventoryUpdated(
            _buyer,
            _productId,
            _desc,
            int256(_price),
            false
        );
    }

    /**
     * This method will be called by the Marketplace after someone has bought a product off this User.
     * The method transfers the money received during the sale to the user's account, and Emits the
     * ProductSold event to inform the client UI of the sale.
     */
    function productSold(
        uint256 _productId,
        address payable _seller,
        string memory _desc,
        uint256 _price
    ) public payable {
        require(
            msg.sender == marketplace,
            "this method can be called only by the marketplace"
        );
        _seller.transfer(msg.value);
        emit UserLedgerUpdated(_seller, now, _desc, int256(_price));
        emit UserInventoryUpdated(
            _seller,
            _productId,
            _desc,
            -1 * int256(_price),
            false
        );
    }

    /**
     * This method is called by the Accounts SmartContract upon creatimg a new user.
     * It simply emits a UserLedgerUpdated event to log the cost of the Account creation in the User's ledger.
     */
    function userCreated(address _user) public {
        emit UserLedgerUpdated(_user, now, "Account created", 0);
    }

    // destructor of the contract
    function kill() public {
        require(
            msg.sender == owner,
            "only the owner of this contract can destroy it"
        );
        selfdestruct(msg.sender);
    }

    // enusre that none transfers anything to the Factory (no undertable ;)
    function() external payable {
        revert("Cannot accept any Ether directly");
    }
}
