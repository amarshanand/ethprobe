pragma solidity 0.5.16;

import "./User.sol";

/**
 * Marketplace is a registry of all products. It provides the ability to Buy / Sell products
 */
contract Marketplace {
    // the owner of the contract
    address owner;

    // a mapping of productId->Product
    struct ProductListing {
        address payable seller;
        string desc;
        uint256 price;
        address payable buyer;
    }
    mapping(uint256 => ProductListing) public productMap;

    // event to notify clientApp of the listing of a new product
    event MarketplaceProductListed(
        uint256 productId,
        uint256 listTime,
        uint256 price,
        string desc,
        uint256 builtTime,
        address seller
    );
    event MarketplaceProductSold(
        uint256 productId,
        address buyer,
        uint256 sellTime
    );

    // constructor
    constructor() public {
        owner = msg.sender;
    }

    /**
     * Listing a Product involves the following:
     * 1. Ensuring that the Product hasnt been listed before
     * 2. Emitting update Events to the clientApp
     */
    function listProduct(
        uint256 _productId,
        uint256 _price,
        string memory _desc,
        uint256 _builtTime,
        address payable _seller
    ) public {
        require(
            productMap[_productId].price == 0,
            "A Product with this id already exists"
        );
        productMap[_productId] = ProductListing(
            _seller,
            _desc,
            _price,
            address(0)
        );
        uint256 listTime = now;
        emit MarketplaceProductListed(
            _productId,
            listTime,
            _price,
            _desc,
            _builtTime,
            _seller
        );
    }

    /**
     * This methid will be called by the User contract. Buying a product involves the following:
     * 1. Ensuring that the Product is listed and is selling (doesnt have a buyer already)
     * 2. Ensuring that the buyer has provided exact amount to buy the product
     * 3. Transfering the amount provided by the buyer to the seller's account
     * 4. Putting the buyers's name against the product
     * 5. Emitting the ProductUnlisted event to notify the client app of the Sale
     */
    function buyProduct(uint256 _productId, address payable _buyer)
        public
        payable
    {
        require(
            productMap[_productId].price != 0,
            "This product is not listed"
        );
        require(
            productMap[_productId].buyer == address(0),
            "This product has been bought already"
        );
        ProductListing storage product = productMap[_productId];
        require(
            product.price * 1000000000000000 == msg.value,
            "You must pay exact price in Finney to buy this product"
        );
        require(
            productMap[_productId].seller != _buyer,
            "You cant buy your own product"
        );
        User(msg.sender).productSold.value(msg.value)(
            _productId,
            product.seller,
            product.desc,
            msg.value
        );
        product.buyer = _buyer;
        User(msg.sender).productBoughtCB(
            _productId,
            _buyer,
            product.desc,
            msg.value
        );
        emit MarketplaceProductSold(_productId, _buyer, now);
    }

    // destructor of the contract
    function kill() public {
        require(
            msg.sender == owner,
            "only the owner of this contract can destroy it"
        );
        selfdestruct(msg.sender);
    }

    // enusre that none transfers anything to the Factory (no undertable ;)
    function() external payable {
        revert("Cannot accept any Ether directly");
    }
}
