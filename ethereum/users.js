/**
 * You must produce EXACTLY ONE FACTORY account (and name it as 'Factory'), and
 * EXACTLY ONE ADMIN account (and name it as 'Admin').
 * All fields (name, about, description etc) are optional, except the 'name' field for Factory and Admin
 */
const seed = {
  users: {
    "0x5965a01e6A5244665b23Fc233A43DA169f3Ef38c": {
      name: "Factory",
      about: "Anything for 10𝔽",
      description: "Lombe's Mill, world's first factory, 18th Century",
      image:
        "https://upload.wikimedia.org/wikipedia/commons/9/9b/Lombe%27s_Mill_print_Darley-Factory_p105.png",
      url: "https://en.wikipedia.org/wiki/Lombe%27s_Mill",
    },
    "0x41721e0bDB135E58cA1082091A1ccc5d020D9971": {
      name: "Admin",
      about: "Uploading Smart Contracts",
      description: "Code of Hammurabi, world's oldest documented law, 1754 BC",
      image:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Prologue_Hammurabi_Code_Louvre_AO10237.jpg/800px-Prologue_Hammurabi_Code_Louvre_AO10237.jpg",
      url: "https://en.wikipedia.org/wiki/Code_of_Hammurabi",
    },
  },
};

module.exports = seed;
