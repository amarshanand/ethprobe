[![Ethprobe - Complete walthrough](./video_image.png)](https://www.youtube.com/watch?v=TWUSuGF7o7g)

# EthProbe - Probing Ethereum through a Marketplace App

This project simulates a marketplace where people can produce, buy a sell products for a price. The objective of the project is to explore the workings of the Ethereum Blockchain and how Dapps can interact with it.

# Documentation

All documentation is available at [EthProbe Wiki](https://gitlab.com/amarsh.hk/ethereum-based-marketplace-simulator/wikis/pages)

Sadly, Gitlab doesnt support putting comments in Wiki, hence, please feel free to edit the page instead.

# Live Demo

[https://amarsh.hk.gitlab.io/ethprobe](https://amarsh.hk.gitlab.io/ethprobe) or [http://ethprobe.com/](http://ethprobe.com/)

# Configuration I - .env files

Before attempting to run this app locally, you need to produce two `.env` files, one in the `/ethereum` folder, and the other in `/dapp` folder:

```shell
# /ethereum/.env

ADMIN_PRIVATE_KEY = 'e66b5...797a5'
INFURA_PROJECT_ID = 'fb69a665...cebe406a9'
```

### Obtaining ADMIN_PRIVATE_KEY

1. Install [Metamask Chrome Extension](https://metamask.io/).
2. Create a Metamask account. This will autometically create one default account for you. We would use this as our `Admin` account.
3. In Metamask, look for `Account Details` of this account.
4. In `Account Details`, rename this account as `Admin`, and click on `Export Private Key`.
5. Reveal and Copy this `Private Key` to the `ADMIN_PRIVATE_KEY` field in `/ethereum/.env`

### Obtaining INFURA_PROJECT_ID

1. Create an account on [Infura](https://infura.io/)
2. Create a new Project
3. Copy the `PROJECT ID` of this new Project and paste it in the `INFURA_PROJECT_ID` field of the `/ethereum/.env` file

```shell
# /dapp/.env

REACT_APP_ETHERSCAN_API_KEY = 'HEWUCA...TM5ZUV42'

# social login APP_IDs
REACT_APP_FACEBOOK_APP_ID='244...226'
REACT_APP_LINKEDIN_APP_ID='81w...5ii'
REACT_APP_GOOGLE_APP_ID='767...00v.apps.googleusercontent.com'
REACT_APP_AMAZON_APP_ID='amzn1.application-oa2-client.d64f...6019'
REACT_APP_GITHUB_APP_ID='e20c...a983'
REACT_APP_INSTAGRAM_APP_ID='9178...6429'
```

### Obtaining REACT_APP_ETHERSCAN_API_KEY

1. Create an account on [Etherscan](https://etherscan.io/apis)
2. On the left panel, look for `Developers > API-KEYs`
3. Create an `API-KEY`, copy it, and paste it in `REACT_APP_ETHERSCAN_API_KEY` field of the `/dapp/.env` file

### Obtaining Social login

To enable Social login, you need to obtain various APP_IDs from their respective provders.

# Configuration II - /ethereum/users.js

`/ethereum/users.js` file contains the info about two critical users for EthProbe - `Admin` and `Factory`. Of these, we have already created the `Admin` user above. To configure this file, you need to put the address of the `Admin` account you have created in Metamask into the entry for `name: 'Admin'` field.

Additionally, you need to create another account in Metamask called `Factory`, and put its address in the `name: 'Factory'` field of the `/ethereum/users.js` file.

Note that the `Factory` would only earn money, hence, we dont need to put any funds in this account.

# Deploying SmartContracts to Ethereum

We are deploying our SmartContracts to [Goerli Testnet](https://goerli.etherscan.io/).

The `ADMIN_PRIVATE_KEY` in the `/ethereum/.env` tells the deployer script of which account to use for deploying the SmartContracts. To put funds into the `Admin` account, go to [MetaMask Ether Faucet](https://goerlifaucet.com/).

Click on `request 1 ether from faucet` button. If is down or errored, you can use [Goerli Ethereum Faucet](https://faucet.goerli.be/) or similar.

Once you have sufficient funds (about 2Eth is enough), run the following from `/ethereum` folder:

```shell
% npm install
% npm run deploy-goerli # for deploying on Goerli
OR
% npm run deploy-matic # for deploying on Polygon
```

# Creating DNS entry for running the Dapp locally

I had some issues with whitelisting callbacks to `http://localhost:3000` in Facebook social login. Hence, I had to create a local DNS entry that appears as a legetimate url to Facebook. In Mac, the steps are:

```shell
# open the /etc/hosts file
% sudo nano /etc/hosts

# and add the following to it
127.0.0.1       www.mylocalmachine.com

# and flush the DNS cache
% sudo killall -HUP mDNSResponder;say DNS cache has been flushed
```

Consequently, you'd notice that `npm run start` attempts to start `https://www.mylocalmachine.com:3000`

# Running the Dapp locally

The EthProbe Dapp is a simple React + MobX App. Ensure that you have `Node v8.11.3` or above. From the `/dapp` folder, run the following:

```shell
% npm install
% npm run start
```

### NET::ERR_CERT_INVALID on Chrome and Brave

Ethprobe can only run on Chrome, Brave and Edge (untested) since Metamask is only supported on these browsers. On Chrome and Brave on my Mac, I got an issue accessing `https://www.mylocalmachine.com:3000`. Chrome would show `NET::ERR_CERT_INVALID`.

A workaround for this is to type `thisisunsafe` on Chrome. Refer to [Chrome: Bypass NET::ERR_CERT_INVALID for development](https://dblazeski.medium.com/chrome-bypass-net-err-cert-invalid-for-development-daefae43eb12) for details

### Linking files to /src folder

React expects all files required by the App to be within its `/src` folder. Hence, we have linked the file `/ethereum/user.js -> /dapp/src/store/user.js` and also the folder `/ethereum/build/contracts -> /dapp/src/blockchain/contracts`. If you seen errors similar to `Module not found: Can't resolve './contracts' in ...`, perform the linking as follows:

```shell
# from /dapp/src/blockchain dir, link /contracts folder
% rm -rf ./contracts
% ln -s ../../../ethereum/build/contracts ./contracts

# also, link the solidity files
% rm -rf ./contracts/src
% ln -s ../../../ethereum/contracts ./contracts/src

# from /dapp/src/store dir, link users.js file
% rm -rf users.js
% ln -s ../../../ethereum/users.js ./users.js
```

### Enabling HTTPS (SSL) on your site

If the CI/CD pipeline succeeds, you should be able to see the project in gitlab-pages at a url similar to https://amarsh.hk.gitlab.io/ethprobe . From that point, I did the following to enable https on my site, since social logins like Facebbok require https.

1. Buy the domain. I have bought ethprobe.org, ethprobe.com and ebmasi.org from Godaddy
2. https://about.gitlab.com/2017/02/07/setting-up-gitlab-pages-with-cloudflare-certificates outlines the steps
3. You need to transfer the domain from Godaddy to Cloudfare. The way to do that is to update the two Name Servers in Godaddy
4. The only catch is ... ensure that you have only an `A` type DNS record. If you put both `A` type and `CNAME`, Gitlab doesnt work
5. Also, Gitlab asks for Domain verification by ading a `TXT` record. You need to add a DNS entry in Cloudfare, as asked by Gitlab
6. After doing everything, it may take upto 15 mins for the new domain to work (Gitlab would give a 404 for a short while)
