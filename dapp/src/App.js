import React, { Component } from "react";
import { MuiThemeProvider, withStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import { observer } from "mobx-react";
import "./App.css";
import theme from "./theme";
import TopNav from "./components/TopNav";
import User from "./components/User";
import Contract from "./components/Contract";
import Product from "./components/Product";
import Store from "./store";
import Notification from "./components/Notification";
import Error from "./components/Error";
import Disqus from "./components/Disqus";
import Help from "./components/Help";
import SocialSharing from "./components/SocialSharing";
import SocialLogin from "./components/signupComponents/SocialLogin";

/**
 * Entry point of the App. We load the users from the config file
 */

const styles = {
  root: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    maxHeight: "100%",
    minWidth: "57rem",
    overflowY: "scroll",
  },
  users: {
    position: "absolute",
    top: "5.1rem",
    left: "0",
    display: "flex",
    justifyContent: "space-around",
    flexWrap: "wrap",
    width: "calc(100% - 28rem)",
    maxHeight: "calc(100% - 12.8rem)",
    overflow: "scroll",
  },
  contracts: {
    position: "absolute",
    bottom: "0",
    left: "0",
    display: "flex",
    justifyContent: "space-around",
    flexWrap: "wrap",
    width: "calc(100% - 28rem)",
    maxWidth: "calc(100% - 28rem)",
    height: "6.8rem",
    overflowX: "scroll",
  },
  products: {
    position: "absolute",
    padding: "2px 0",
    top: "6rem",
    right: "0",
    display: "flex",
    justifyContent: "space-around",
    flexWrap: "wrap",
    width: "28rem",
    height: "calc(100% - 12.8rem)",
    maxHeight: "calc(100% - 12.8rem)",
    overflow: "scroll",
    textAlign: "center",
  },
  noProducts: {
    opacity: 0.8,
    marginTop: "10rem",
  },
  noProductsMessage: {
    color: "rgb(225,0,80)",
    fontSize: "0.8rem",
    letterSpacing: "1px",
  },
  accountsLoading: {
    width: "100%",
    height: "6em",
    marginTop: "13em",
    background: "url(img/loading.gif) no-repeat center center",
    backgroundSize: "contain",
  },
  marketplaceLoading: {
    width: "6em",
    marginTop: "18em",
    marginLeft: "calc(100% - 18em)",
  },
};

@observer
class App extends Component {
  render() {
    const { classes } = this.props;
    if (Store.crashMessage || window.screen.width < 768) return <Error />;
    // we show the loggedIn user as the first
    let users = [];
    Object.entries(Store.users).forEach(([hash, user]) => {
      const jsx = <User key={hash} hash={hash} />;
      const isThisLoggedInUser = user.hash === Store.loggedInUser;
      const regex = new RegExp(`\\b${Store.searchKW.replace(/[\W_]+/g, " ").replace(/ /g, "|\\b")}`, "gi");
      const isMatch = Store.searchKW === "" || regex.test(`${user.name} ${user.about} ${user.description}`);
      isMatch && isThisLoggedInUser && users.unshift(jsx);
      isMatch && !isThisLoggedInUser && users.push(jsx);
    });
    return (
      <MuiThemeProvider theme={theme}>
        <TopNav />
        <div className={classes.root}>
          {/* list of users and Contracts*/}
          <div className={classes.users}>
            {Store.accountsLoading && <div className={classes.accountsLoading} />}
            {!Store.accountsLoading && users}
          </div>
          <div className={classes.contracts}>
            {Store.contractList.map((contract) => (
              <Contract key={contract.name} name={contract.name} address={contract.address} />
            ))}
          </div>
          {/* list of products (marketplace) */}
          {!Store.marketplaceLoading && (
            <div className={classes.products}>
              {Store.productList.length <= 0 && (
                <div>
                  <Typography variant="h5" className={classes.noProducts}>
                    No Products in the Market
                  </Typography>
                  <Typography variant="body2" className={classes.noProductsMessage}>
                    Please produce something to start trading
                  </Typography>
                </div>
              )}
              {Store.productList.length > 0 && (
                <div>
                  {Store.productList.map((product) => (
                    <Product key={product.pid} pid={product.pid} />
                  ))}
                </div>
              )}
            </div>
          )}
          {Store.marketplaceLoading && <img src="img/loading.gif" className={classes.marketplaceLoading} />}
        </div>
        {/* Show / hide Help */}
        <Help />
        {/* Social login (defaults to Signup page if the user wants to stay anynymous) */}
        <SocialLogin />
        {/* Social sharing  */}
        <SocialSharing />
        {/* Discussion forum (https://disqus.com)  */}
        <Disqus />
        {/* Notifications */}
        <Notification />
      </MuiThemeProvider>
    );
  }
}

export default withStyles(styles)(App);
