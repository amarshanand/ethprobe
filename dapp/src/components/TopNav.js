import React, { useRef } from 'react';
import Avatar from '@material-ui/core/Avatar';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Info from '@material-ui/icons/HelpOutlineOutlined';
import Tooltip from '@material-ui/core/Tooltip';
import { fade } from '@material-ui/core/styles/colorManipulator';
import SearchIcon from '@material-ui/icons/Search';
import ClearIcon from '@material-ui/icons/Clear';
import InputBase from '@material-ui/core/InputBase';
import Icon from '@mdi/react';
import { mdiGithubCircle, mdiDisqusOutline, mdiAccountOff } from '@mdi/js';
import Store from '../store';
import { projectRepo } from '../config';
import { observer } from 'mobx-react';

/**
 * The TopNav bar. State of the App can be changed by updaing the Store, which would simply broadcast the new state to all @observers
 */

const styles = (theme) => ({
    root: {
        flexGrow: 1
    },
    grow: {
        flexGrow: 1,
        paddingLeft: '1rem'
    },
    button: {
        color: 'white'
    },
    seal: {
        width: '2rem'
    },
    avatar: {
        marginRight: '1rem',
        width: '1.5rem',
        height: '1.5rem',
        display: 'inline-block',
        verticalAlign: 'bottom',
        border: '2px solid white',
        boxSizing: 'border-box'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25)
        },
        marginRight: theme.spacing.unit * 2,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing.unit * 3,
            width: 'auto'
        }
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    clearIcon: {
        position: 'absolute',
        color: 'white',
        top: 0,
        right: 0,
        padding: '4.5px',
        transform: 'scale(0.75)'
    },
    inputRoot: {
        color: 'inherit',
        width: '100%'
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10,
        marginRight: '1.5rem',
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200
        }
    }
});

@observer
class TopNav extends React.Component {
    constructor(props) {
        super(props);
        this.state = { anchorElMenu: null };
    }

    render() {
        const { classes } = this.props;
        const user = Store.users[Store.loggedInUser];
        let name = '';
        let image = 'img/seal.png';
        if (user) {
            name = user.name;
            image = user.image;
        }
        return (
            <div className={classes.root}>
                <AppBar>
                    <Toolbar>
                        <img alt="seal" src="img/seal.png" className={classes.seal} />
                        <Typography variant="h6" color="inherit" className={classes.grow}>
                            EthProbe - Ethereum based Marketplace
                        </Typography>
                        {/* Search */}
                        <div className={classes.search}>
                            <div className={classes.searchIcon}>
                                <SearchIcon />
                            </div>
                            <InputBase
                                placeholder="Search…"
                                classes={{
                                    root: classes.inputRoot,
                                    input: classes.inputInput
                                }}
                                value={Store.searchKW}
                                onChange={(e) => (Store.searchKW = e.target.value)}
                            />
                            <IconButton
                                className={classes.clearIcon}
                                onClick={() => (Store.searchKW = '')}
                                disabled={Store.searchKW === ''}
                            >
                                <Tooltip title="clear">
                                    <ClearIcon />
                                </Tooltip>
                            </IconButton>
                        </div>
                        {/* User Avatar*/}
                        {user && (
                            <IconButton disabled={true}>
                                <Tooltip title={`${name} [LoggedIn from Metamask]`}>
                                    <Avatar alt={name} src={image} className={classes.avatar} style={{ margin: 0 }} />
                                </Tooltip>
                            </IconButton>
                        )}
                        {/* Register Icon */}
                        {!user && (
                            <IconButton onClick={() => (Store.showSignup = true)}>
                                <Tooltip title={`click to register`}>
                                    <Icon path={mdiAccountOff} size={1.1} color="white" className={classes.button} />
                                </Tooltip>
                            </IconButton>
                        )}
                        {/* Gitlab project link */}
                        <IconButton onClick={() => window.open(projectRepo, '_blank')}>
                            <Tooltip title="View Source Code on Gitlab">
                                <Icon path={mdiGithubCircle} size={1.1} color="white" className={classes.button} />
                            </Tooltip>
                        </IconButton>
                        {/* Disqus forum */}
                        <IconButton onClick={() => (Store.showDisqus = !Store.showDisqus)}>
                            <Tooltip title="View discussion forum on Disqus">
                                <Icon path={mdiDisqusOutline} size={1.1} color="white" className={classes.button} />
                            </Tooltip>
                        </IconButton>
                        {/* Help Icon */}
                        <IconButton onClick={() => (Store.showHelp = !Store.showHelp)}>
                            <Tooltip title="Help">
                                <Info className={classes.button} />
                            </Tooltip>
                        </IconButton>
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

TopNav.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(TopNav);
