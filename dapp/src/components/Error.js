import React from "react";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import SocialSharing from "./SocialSharing";
import Store from "../store";

/**
 * A cool way of diplaying errors ... Windows style, decorated with hexspeaks (https://en.wikipedia.org/wiki/Hexspeak)
 */

const styles = {
  blueScreenOfDeath: {
    padding: "2em",
    fontFamily: '"Lucida Console", Monaco, monospace',
    color: "white",
  },
  crashMessage: {
    color: "white !important",
    margin: "6rem 0 4rem 0",
  },
  cheveron: {
    fontSize: "1.5rem",
  },
  hexCodes: {
    cursor: "pointer",
    width: "60%",
  },
  videos: {
    position: "absolute",
    top: "1rem",
    right: "1rem",
    width: "560px",
    height: "315px",
  },
};

class Error extends React.Component {
  render() {
    const { classes } = this.props;
    document.body.style.background = "rgb(41,0,173)";
    return (
      <div className={classes.blueScreenOfDeath}>
        <div
          className={classes.hexCodes}
          onClick={() =>
            window.open("https://en.wikipedia.org/wiki/Hexspeak", "_blank")
          }
        >
          {hexCodes}
        </div>
        <div
          className={classes.crashMessage}
          dangerouslySetInnerHTML={{ __html: `FATAL : ${Store.crashMessage}` }}
        />
        C:\<span className={classes.cheveron}>›</span>{" "}
        <span className="blinking-cursor">█</span>
        <iframe
          className={classes.videos}
          src="https://www.youtube.com/embed/videoseries?list=PL3FOIyArJIEPZvpNtJ7dJQX-Q_wERngmt"
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        />
        <SocialSharing left={true} />
      </div>
    );
  }
}

Error.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Error);

const shuffle = (a) => {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
};

const hexCodes = shuffle([
  "0x000FF1CE",
  "0x00BAB10C",
  "0x8BADF00D",
  "0x1BADB002",
  "0x1CEB00DA",
  "0xB105F00D",
  "0xB16B00B5",
  "0xBAAAAAAD",
  "0xBAADF00D",
  "0xBAD22222",
  "0xBAADA555",
  "0xBADDCAFE",
  "0xC00010FF",
  "0xC0DED00D",
  "0xCAFEBABE",
  "0xCAFED00D",
  "0xCEFAEDFE",
  "0x0D15EA5E",
  "0xDABBAD00",
  "0xDEADBAAD",
  "0xDEADBABE",
  "0xDEADBEAF",
  "0xDEADC0DE",
  "0xDEADDEAD",
  "0xDEADD00D",
  "0xDEADFA11",
  "0xDEAD10CC",
  "0xDEADFEED",
  "0xDEFEC8ED",
  "0xE011CFD0",
  "0xfaceb00c",
  "0xFACEFEED",
  "0xFBADBEEF",
  "0xFEE1DEAD",
  "0xFEEDBABE",
  "0xFEEDC0DE",
  "0xFFBADD11",
  "0xD0D0CACA",
  "0x1337BABE",
]).join(" ");
