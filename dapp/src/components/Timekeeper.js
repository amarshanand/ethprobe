import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { observer } from 'mobx-react';

const styles = (theme) => ({
    root: {
        display: 'inline-block',
        background: 'rgb(40,40,40)',
        borderRadius: '4px',
        fontSize: '0.85rem',
        fontFamily: 'digital',
        fontWeight: 'bold',
        letterSpacing: '3px',
        width: '1.55rem',
        color: 'rgb(93, 255, 0)',
        margin: '0.35rem 0.2em 0 0.4rem',
        padding: '3px 5px 2px 5px',
        textAlign: 'right'
    }
});

@observer
class Timekeeper extends Component {
    componentDidMount() {
        const { user, hash } = this.props;
        this.timer = setInterval(() => user.pendingTxn[hash].counter++, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    render() {
        const { classes, user, hash } = this.props;
        const { counter } = user.pendingTxn[hash];
        return <div className={classes.root}>{`${counter <= 9 ? '00' : counter <= 99 ? '0' : ''}${counter}`}</div>;
    }
}

Timekeeper.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Timekeeper);
