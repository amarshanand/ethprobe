import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import LockIcon from '@material-ui/icons/Settings';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Store from '../../store';
import { observer } from 'mobx-react';
import { Typography } from '@material-ui/core';
import { fakeNameGenerator } from '../../config';
import Axios from 'axios';
import Blockchain from '../../blockchain';

/**
 * Signup page is required to identify a user and allow her to trade in EthProbe.
 * The only mandatory field is the user's Ethereum address (injected by Metamask). Other fields will be Auto Populated for ease of signup.
 * Signup is optional, and the user can view EthProbe without one. But they cannot transact without signing-up first.
 * Signup get activated automatically if we detect that the user has logged into Metamask with an accountId unknown to us. 
 */

const styles = {
    lock: {
        position: 'absolute',
        top: '3rem',
        right: '3rem',
        width: '2rem',
        height: '2rem'
    },
    half_left: {
        width: 'calc(50% - 1em)',
        margin: '0 1em 0 0'
    },
    half_right: {
        width: 'calc(50% - 1em)',
        margin: '0 0 0 1em'
    },
    metamask: {
        width: '1.4rem',
        marginRight: '0.8rem',
        verticalAlign: 'text-top'
    },
    message: {
        marginBottom: '1rem'
    },
    textField: {
        marginTop: '1rem'
    },
    avatar: {
        position: 'absolute',
        top: '1rem',
        right: '1.1em',
        width: '6em',
        height: '6em',
        borderRadius: '6px',
        backgroundSize: 'cover'
    }
};

@observer
class Signup extends React.Component {
    // fakePerson can assume three states - {REQUIRED, NOT_REQUIRED, FETCHING}
    state = {
        fakePerson: 'REQUIRED',
        name: '',
        about: '',
        description: '',
        image: '',
        url: ''
    };

    async getFakePerson() {
        if (this.state.fakePerson === 'NOT_REQUIRED') return;
        this.setState({ fakePerson: 'FETCHING' });
        // generate a fake person
        const { data: { results } } = await Axios(fakeNameGenerator);
        const [ user ] = results;
        user.name = `${cap(user.name.first)} ${cap(user.name.last)}`;
        user.about = `${user.email}`;
        user.image = user.picture.large;
        // whatever we generate would be overwritten by what had come from Social Login
        this.setState({
            fakePerson: 'REQUIRED',
            ...user,
            ...this.props.user
        });
    }

    componentDidMount() {
        const { name, about, image } = this.props.user;
        this.setState({ fakePerson: name && about && image ? 'NOT_REQUIRED' : 'REQUIRED', ...this.props.user }, () =>
            this.getFakePerson()
        );
    }

    handleChange = (name) => (event) => {
        this.setState({
            [name]: event.target.value
        });
    };

    doSignup() {
        Store.showSignup = false;
        Blockchain.doSignup(this.state);
    }

    render() {
        const { classes } = this.props;
        const { fakePerson, name, about, description, image, url } = this.state;
        const loading = fakePerson === 'FETCHING';
        return (
            <Dialog maxWidth={false} open={Store.showSignup} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">
                    <img src="img/metamask.png" className={classes.metamask} />Verify your details
                </DialogTitle>
                {!loading && <div style={{ backgroundImage: `url(${image})` }} className={classes.avatar} />}
                {loading && <LockIcon color="error" className={`${classes.lock} rotate`} />}
                <DialogContent style={{ position: 'relative' }}>
                    <Typography className={classes.message} variant="caption" color="secondary">
                        {fakePerson === 'NOT_REQUIRED' && 'Please provide some more info about yourself'}
                        {fakePerson === 'REQUIRED' &&
                            'We have generated some fake info about you, but we encourage you to make them real'}
                        {fakePerson === 'FETCHING' && 'Generating fake info ...'}
                    </Typography>
                    <TextField
                        className={`${classes.textField} ${classes.half_left}`}
                        id="address"
                        label="Ethereum Address"
                        disabled
                        value={Store.loggedInUser}
                    />
                    <TextField
                        className={`${classes.textField} ${classes.half_right}`}
                        id="image"
                        label="Picture"
                        type="url"
                        fullWidth
                        disabled={loading}
                        value={image}
                        onChange={this.handleChange('image')}
                    />
                    <TextField
                        className={`${classes.textField} ${classes.half_left}`}
                        autoFocus
                        id="name"
                        label="Name"
                        disabled={loading}
                        value={name}
                        onChange={this.handleChange('name')}
                    />
                    <TextField
                        className={`${classes.textField} ${classes.half_right}`}
                        id="about"
                        label="Email"
                        fullWidth
                        disabled={loading}
                        value={about}
                        onChange={this.handleChange('about')}
                    />
                    <TextField
                        className={`${classes.textField} ${classes.half_left}`}
                        id="description"
                        label="About"
                        fullWidth
                        disabled={loading}
                        value={description}
                        onChange={this.handleChange('description')}
                    />
                    <TextField
                        className={`${classes.textField} ${classes.half_right}`}
                        id="url"
                        label="Webpage"
                        type="url"
                        fullWidth
                        disabled={loading}
                        value={url}
                        onChange={this.handleChange('url')}
                    />
                </DialogContent>
                <DialogActions>
                    {fakePerson !== 'NOT_REQUIRED' && (
                        <Button onClick={() => this.getFakePerson()} color="primary" disabled={loading}>
                            Try another
                        </Button>
                    )}
                    <Button
                        onClick={() => this.doSignup()}
                        color="primary"
                        disabled={!name || !image || !about || loading}
                    >
                        Register
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

// capitalise the first letter of the passed string
const cap = (string) => string.charAt(0).toUpperCase() + string.slice(1);

Signup.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Signup);
