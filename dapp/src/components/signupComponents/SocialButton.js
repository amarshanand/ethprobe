import React from 'react';
import SocialLogin from 'react-social-login';

const Button = ({ children, triggerLogin, ...props }) => (
    <button className="socialButton" onClick={triggerLogin} {...props}>
        <div
            className="socialIcon"
            style={{ background: `url(img/social/${props.name}.png)`, backgroundSize: 'cover' }}
        >
            {children}
        </div>
    </button>
);

export default SocialLogin(Button);
