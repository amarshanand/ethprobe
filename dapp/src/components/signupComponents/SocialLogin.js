import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import { observer } from "mobx-react";

import Blockchain from "../../blockchain";
import Store from "../../store";
import SocialButton from "./SocialButton";
import Signup from "./Signup";
import { minBalance } from "../../config";

/**
 * Social login encourages the user to use his social credentials to create a user. After collecting the info
 * (or not collecting anything at-all, if the user chosses to stay anynymous), Signup is called with the user's info.
 */

const styles = {
  root: {},
  metamask: {
    width: "1.4rem",
    marginRight: "0.8rem",
    verticalAlign: "text-top",
  },
  socialButtons: {
    display: "flex",
    justifyContent: "space-between",
    margin: "1rem 0 0",
    border: "3px solid transparent",
  },
  lowBalance: {
    color: "rgb(200,0,0)",
    height: "8.8rem",
    padding: "1rem 1.5rem 0 1.5rem",
    border: "1px solid rgb(200,0,0)",
    borderRadius: "0.3rem",
    lineHeight: "2rem",
  },
  link: {
    color: "rgb(0,0,180)",
    fontSize: "0.8rem",
    cursor: "pointer",
    textDecoration: "none",
    paddingBottom: "2px",
    borderBottom: "1px solid rgb(0,0,180)",
  },
  message: {
    color: "rgb(0,120,0)",
    fontSize: "0.8rem",
  },
};

@observer
class SocialSignup extends React.Component {
  state = { showSignup: false, provider: null, lowBalance: false };
  handleSocialLogin(user) {
    const { provider, profile } = user;
    let _user = {};
    if (profile.profilePicURL) _user.image = profile.profilePicURL;
    this.setState({ showSignup: true, provider, ..._user });
  }

  handleSocialLoginFailure(err) {
    Store.doNotify(`Error in Social login`, "error");
    console.log(err);
  }

  render() {
    const { classes } = this.props;
    const { showSignup, lowBalance } = this.state;
    Blockchain.getBalanceForAddress(Store.loggedInUser, (balance) =>
      this.setState({ lowBalance: balance <= minBalance })
    );
    return showSignup ? (
      <Signup user={this.state} />
    ) : (
      <Dialog
        maxWidth={false}
        open={Store.showSignup}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          <img src="img/metamask.png" className={classes.metamask} />
          Register with EthProbe
        </DialogTitle>
        <DialogContent style={{ position: "relative" }}>
          <div className={classes.socialButtons}>
            {lowBalance && (
              <div className={classes.lowBalance}>
                Your Account balance is low. Get some free ethers by clicking on
                <br />
                <a
                  className={classes.link}
                  href="https://goerlifaucet.com"
                  target="_blank"
                >
                  https://goerlifaucet.com
                </a>
                <br />
                <a
                  className={classes.link}
                  href="https://faucet.quicknode.com"
                  target="_blank"
                >
                  https://faucet.quicknode.com
                </a>
                <div className={classes.message}>
                  It may take a short while to get the funds into your account.
                  Leave this page open.
                </div>
              </div>
            )}
            {!lowBalance &&
              [
                "facebook",
                "google",
                "linkedin",
                "amazon",
                "instagram" /*, 'github'*/,
              ].map((name) => (
                <SocialButton
                  key={name}
                  redirect={window.location.toString()}
                  name={name}
                  provider={name}
                  appId={process.env[`REACT_APP_${name.toUpperCase()}_APP_ID`]}
                  onLoginSuccess={(user) => this.handleSocialLogin(user)}
                  onLoginFailure={(err) => this.handleSocialLoginFailure(err)}
                />
              ))}
          </div>
        </DialogContent>
        <DialogActions>
          {!lowBalance && (
            <Button
              onClick={() => this.setState({ showSignup: true })}
              color="primary"
              disabled={lowBalance}
            >
              Register as Guest
            </Button>
          )}
        </DialogActions>
      </Dialog>
    );
  }
}

SocialSignup.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SocialSignup);
