import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Close from '@material-ui/icons/Cancel';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import Slide from '@material-ui/core/Slide';
import { observer } from 'mobx-react';
import { DiscussionEmbed } from 'disqus-react';
import { disqus } from '../config';
import Store from '../store';

/**
 * A discussion forum, powered by https://disqus.com/
 */

const styles = {
    root: {
        position: 'fixed',
        width: '100%',
        minHeight: '40%',
        height: '40%',
        maxHeight: '40%',
        bottom: 0,
        left: 0,
        padding: '2.5rem 1.5rem 1rem 1.5rem',
        background: 'rgb(252,252,252)',
        boxShadow: '0px 0px 4px 4px rgb(220,220,220) !important',
        zIndex: 1
    },
    container: {
        width: 'calc(100% - 3rem)',
        maxHeight: 'calc(100% - 0.5rem)',
        overflow: 'scroll'
    },
    close: {
        position: 'fixed',
        bottom: 'calc(100% - 3rem)',
        right: '3rem',
        color: 'red',
        zIndex: 2
    }
};

@observer
class Disqus extends React.Component {
    render() {
        const { classes } = this.props;
        const { shortname, url, identifier, title } = disqus;
        const disqusConfig = { url, identifier, title };
        return (
            <Slide direction="up" in={Store.showDisqus} mountOnEnter unmountOnExit>
                <Paper className={classes.root}>
                    <IconButton className={classes.close} onClick={() => (Store.showDisqus = false)}>
                        <Tooltip title="Close">
                            <Close />
                        </Tooltip>
                    </IconButton>
                    <div className={classes.container}>
                        <DiscussionEmbed shortname={shortname} config={disqusConfig} />
                    </div>
                </Paper>
            </Slide>
        );
    }
}

Disqus.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Disqus);
