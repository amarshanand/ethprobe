import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { observer } from 'mobx-react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import InventoryIcon from '@material-ui/icons/Assignment';
import LedgerIcon from '@material-ui/icons/ChromeReaderMode';
import LockIcon from '@material-ui/icons/Settings';
import EtherscanIcon from '@material-ui/icons/Language';
import MinimizeIcon from '@material-ui/icons/HighlightOff';
import BuyIcon from '@material-ui/icons/ShoppingCart';
import Icon from '@mdi/react';
import { mdiFactory } from '@mdi/js';

import Store from '../store';
import Produce from './actionComponents/Produce';
import Buy from './actionComponents/Buy';
import Inventory from './actionComponents/Inventory';
import Ledger from './actionComponents/Ledger';
import Timekeeper from './Timekeeper';
import { costOfProduction, etherscanURL } from '../config';

/**
 * A User is our key entitity. A list of Users ie being loaded from ./config. 
 * In the first interation, each user is preloaded with a fixed amount of money in his wallet. 
 * The user can choose to use his money to produce products (will be loaded from ./config), which 
 * come with a fixed price. The User can choose to sell if for whatever price he wishes to.
 * In the second iteration, we would replace the user from config with real Ethereum Testnet users,
 * and all transactions will occur by executing SmartContracts. 
 */

const styles = {
    root: {
        minWidth: '25rem',
        xwidth: 'calc(50% - 3rem)',
        width: '100%',
        display: 'inline-block',
        margin: '1rem',
        position: 'relative'
    },
    rootSummarised: {
        minWidth: '12.5rem',
        width: 'auto',
        maxHeight: '5.85rem'
    },
    flash: {
        backgroundColor: 'rgba(33,150,243, 0.2)',
        transition: 'background-color 0.1s'
    },
    noFlash: {
        backgroundColor: 'inherit',
        transition: 'background-color 1.5s'
    },
    highlight: {
        boxShadow: '0px 0px 3px 3px rgba(245,14,77, 0.8)',
        transition: 'box-shadow 0.1s'
    },
    intro: {
        display: 'inline-block'
    },
    hash: {
        fontSize: 11,
        letterSpacing: 1,
        opacity: 0.7,
        cursor: 'pointer'
    },
    canTransact: {
        background: 'rgba(0,0,0,0.03)'
    },
    title: {
        fontSize: 18,
        letterSpacing: 1
    },
    about: {
        fontSize: 13,
        letterSpacing: 1,
        opacity: 0.7,
        marginTop: -2
    },
    balance: {
        fontSize: '0.8em',
        fontWeight: 'bold',
        letterSpacing: '1px',
        lineHeight: '1rem',
        marginRight: '0.175rem'
    },
    inventory: {
        color: 'blue',
        lineHeight: '1.5rem'
    },
    purchases: {
        color: 'darkgreen',
        lineHeight: '1.5rem'
    },
    pendingTxn: {
        display: 'inline-block',
        cursor: 'pointer'
    },
    expandClosed: {
        color: 'rgba(0,0,0,0.5)',
        fill: 'rgba(0,0,0,0.5)'
    },
    expandOpen: {
        color: 'rgb(0, 122, 212)',
        fill: 'rgb(0, 122, 212)'
    },
    avatar: {
        marginRight: 20,
        width: 60,
        height: 60,
        display: 'inline-block',
        verticalAlign: 'bottom',
        boxShadow: '0px 0px 3px 3px rgba(160,160,160,1)'
    },
    pointer: {
        cursor: 'pointer'
    },
    stats: {
        position: 'absolute',
        top: '1rem',
        right: '1.5rem',
        textAlign: 'right'
    },
    timers: {
        position: 'absolute',
        top: '5rem',
        right: '1.5rem'
    },
    summarisedTimer: {
        top: '3.44rem'
    },
    lock: {
        position: 'absolute',
        top: '1rem',
        right: '1.5rem'
    },
    minmax: {
        position: 'absolute',
        opacity: 1,
        top: 0,
        left: 0,
        padding: 0,
        transform: 'scale(0.75)',
        transitionProperty: 'all',
        transitionDuration: '0.3s'
    },
    summarised: {
        opacity: 0.6,
        transform: 'scale(0.75) rotate(45deg)'
    },
    viewInEtherscan: {
        position: 'absolute',
        right: '0.5em'
    }
};

@observer
class User extends Component {
    state = { summarised: true, expanded: { buy: false, produce: false, inventory: false, ledger: false } };

    handleExpandClick = (type) => {
        this.setState({
            expanded: { ...this.state.expanded, [type]: !this.state.expanded[type] }
        });
    };

    closeAction = (type) => {
        this.setState({
            expanded: { ...this.state.expanded, [type]: false }
        });
    };

    render() {
        const { classes, hash } = this.props;
        const { summarised } = this.state;
        const { buy, produce, inventory, ledger } = this.state.expanded;
        const user = Store.users[hash];
        const isLedgerOnly = hash === Store.factoryHash || hash === Store.adminHash;
        const canTransact = Store.loggedInUser == hash;
        return (
            <Card
                className={`${classes.root} ${summarised ? classes.rootSummarised : ''} 
                ${!Store.flash[hash] && canTransact ? classes.canTransact : ''} 
                ${Store.flash[hash] ? classes.flash : classes.noFlash} 
                ${Store.highlightUser && Store.highlightUser.toString().includes(hash) ? classes.highlight : ''}`}
            >
                <CardContent>
                    {/* minimize / maximize button to toggle between summary and fullview mode */}
                    <IconButton
                        className={`${classes.minmax} ${summarised ? classes.summarised : ''}`}
                        onClick={() => this.setState({ summarised: !summarised })}
                        aria-label={summarised ? 'View Details' : 'View Summary'}
                        disableRipple={true}
                    >
                        <Tooltip title={summarised ? 'View Details' : 'View Summary'}>
                            <MinimizeIcon />
                        </Tooltip>
                    </IconButton>
                    {/* this lock appears when the user has submitted a txn and is waiting for the result */}
                    {user.locked && (
                        <Tooltip title="User Locked. Please wait.">
                            <LockIcon color="error" className={`${classes.lock} rotate`} />
                        </Tooltip>
                    )}
                    {/* wallet balance, inventory and storage */}
                    {!user.locked && (
                        <div className={classes.stats}>
                            <Tooltip title="Wallet balance">
                                <Typography variant="caption" color="secondary" className={classes.balance}>
                                    {user.balance}𝔽
                                </Typography>
                            </Tooltip>
                            {!isLedgerOnly && (
                                <div>
                                    <Tooltip title="Inventory" className={classes.inventory}>
                                        <Typography variant="caption">
                                            {user.stats.assetCount} / {user.stats.assetValue}𝔽 ⇧
                                        </Typography>
                                    </Tooltip>
                                    {!summarised && (
                                        <Tooltip title="Purchases" className={classes.purchases}>
                                            <Typography variant="caption">
                                                {user.stats.purchaseCount} / {user.stats.purchaseValue}𝔽 ⇩
                                            </Typography>
                                        </Tooltip>
                                    )}
                                </div>
                            )}
                        </div>
                    )}
                    {/* timer while waiting for the txn to be mined */}
                    <div className={`${classes.timers} ${summarised ? classes.summarisedTimer : ''}`}>
                        {Object.entries(user.pendingTxn).map(([ hash, { caption } ]) => (
                            <div
                                className={classes.pendingTxn}
                                key={hash}
                                onClick={() => window.open(`${etherscanURL}/tx/${hash}`, '_blank')}
                            >
                                <Tooltip title={`Pending Txn for ${caption}. Click to view in Etherscan`}>
                                    <Typography variant="caption">
                                        <Timekeeper user={user} hash={hash} />
                                    </Typography>
                                </Tooltip>
                            </div>
                        ))}
                    </div>
                    {/* profile picture - taken from linkedin with prior permission */}
                    <Tooltip title={summarised ? user.name : user.description}>
                        <Avatar
                            alt={user.name}
                            src={user.image}
                            className={`${classes.avatar} ${user.url ? classes.pointer : ''}`}
                            onClick={() => user.url && window.open(`${user.url}`, '_blank')}
                        />
                    </Tooltip>
                    {/* short intro. display ethereum hash instead of standard email */}
                    {!summarised && (
                        <div className={classes.intro}>
                            <Tooltip title="Ethereum address. Click to copy">
                                <div className={classes.hash} onClick={() => Store.copyToClipboard(user.hash)}>
                                    {`${user.hash.slice(0, 6)}...${user.hash.slice(-4)}`}
                                </div>
                            </Tooltip>
                            <Typography className={classes.title}>{user.name}</Typography>
                            <Typography className={classes.about}>{user.about}</Typography>
                        </div>
                    )}
                </CardContent>
                {/* we provide four actions - Produce and Buy, View Inventory and Ledger */}
                {!summarised && (
                    <CardActions className={classes.actions} disableActionSpacing>
                        {canTransact &&
                        !isLedgerOnly && (
                            <div>
                                <IconButton
                                    className={this.state.expanded.produce ? classes.expandOpen : classes.expandClosed}
                                    onClick={() => this.handleExpandClick('produce')}
                                    aria-expanded={this.state.expanded.ledger}
                                    aria-label="Produce"
                                    disabled={user.locked || user.balance < costOfProduction}
                                >
                                    <Tooltip title="Produce">
                                        <Icon path={mdiFactory} size={1} />
                                    </Tooltip>
                                </IconButton>
                                <IconButton
                                    className={this.state.expanded.buy ? classes.expandOpen : classes.expandClosed}
                                    onClick={() => this.handleExpandClick('buy')}
                                    aria-expanded={this.state.expanded.buy}
                                    aria-label="Buy"
                                    disabled={user.locked || Store.buyableProductsFor(hash).length <= 0}
                                >
                                    <Tooltip title="Buy">
                                        <BuyIcon />
                                    </Tooltip>
                                </IconButton>
                            </div>
                        )}
                        {!isLedgerOnly && (
                            <IconButton
                                className={`${classes.expand} ${this.state.expanded.inventory
                                    ? classes.expandOpen
                                    : classes.expandClosed}`}
                                onClick={() => this.handleExpandClick('inventory')}
                                aria-expanded={this.state.expanded.inventory}
                                aria-label="Inventory"
                                disabled={user.locked}
                            >
                                <Tooltip title="Inventory">
                                    <InventoryIcon />
                                </Tooltip>
                            </IconButton>
                        )}
                        <IconButton
                            className={`${classes.expand} ${this.state.expanded.ledger
                                ? classes.expandOpen
                                : classes.expandClosed}`}
                            onClick={() => this.handleExpandClick('ledger')}
                            aria-expanded={this.state.expanded.ledger}
                            aria-label="Ledger"
                            disabled={user.locked}
                        >
                            <Tooltip title="Ledger">
                                <LedgerIcon />
                            </Tooltip>
                        </IconButton>
                        <IconButton
                            className={classes.viewInEtherscan}
                            onClick={() => window.open(`${etherscanURL}/address/${user.hash}`, '_blank')}
                            aria-label="View in Etherscan"
                        >
                            <Tooltip title="View in Etherscan">
                                <EtherscanIcon />
                            </Tooltip>
                        </IconButton>
                    </CardActions>
                )}
                {/* action components */}
                <div>
                    <Produce show={!summarised && produce} hash={hash} closeAction={this.closeAction} />
                    <Buy show={!summarised && buy} hash={hash} closeAction={this.closeAction} />
                    <Inventory show={!summarised && inventory} hash={hash} closeAction={this.closeAction} />
                    <Ledger show={!summarised && ledger} hash={hash} closeAction={this.closeAction} />
                </div>
            </Card>
        );
    }
}

User.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(User);
