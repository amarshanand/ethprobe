import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { observer } from "mobx-react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import EtherscanIcon from "@material-ui/icons/Language";
import CodeIcon from "@material-ui/icons/Code";
import PropTypes from "prop-types";

import Store from "../store";
import SourceCode from "./SourceCode";
import { etherscanURL } from "../config";

/**
 * A collection of Products form the marketplace. Products can assume {SELLING,SOLD} states.
 * Each Product would have {name, status, seller, price, producedAt, seller, soldAt}
 */

const styles = {
  root: {
    width: "12.8rem",
    position: "relative",
    maxHeight: "4.6rem",
    margin: "0.45rem 0",
    backgroundColor: "rgba(0,0,0,0)",
  },
  price: {
    position: "absolute",
    top: "0.8rem",
    right: "2rem",
    color: "rgb(225, 0, 80)",
    fontSize: "0.9rem",
    fontWeight: "bold",
    letteSpacing: "2px",
  },
  title: {
    fontSize: 18,
    letterSpacing: 1,
  },
  hash: {
    fontSize: 11,
    letterSpacing: 1,
    opacity: 0.7,
    cursor: "pointer",
  },
  indicator: {
    position: "absolute",
    top: "0.5rem",
    right: "0.5rem",
    width: "8px",
    height: "8px",
    borderRadius: "50%",
  },
  flash: {
    boxShadow: "0px 0px 1px 1px rgb(11,102,35)",
    backgroundColor: "rgb(0, 230,83)",
    transition: "background-color 0.1s",
  },
  noFlash: {
    boxShadow: "none",
    backgroundColor: "transparent",
    transition: "background-color 0.1s",
  },
  viewSource: {
    position: "absolute",
    top: "0",
    right: "0",
    marginTop: "-0.2rem",
  },
  viewInEtherscan: {
    position: "absolute",
    bottom: "0",
    right: "0",
    marginBottom: "-0.2rem",
  },
};

@observer
class Contract extends Component {
  state = { viewSource: false };
  render() {
    const { classes, name, address } = this.props;
    return (
      <Card className={classes.root}>
        <CardContent>
          <Tooltip title="Ethereum address. Click to copy">
            <div
              className={classes.hash}
              onClick={() => Store.copyToClipboard(address)}
            >
              {`${address.slice(0, 6)}...${address.slice(-4)}`}
            </div>
          </Tooltip>
          <Tooltip title="Contract invocation indicator">
            <div
              className={`${classes.indicator} ${
                Store.flash[address] ? classes.flash : classes.noFlash
              }`}
            />
          </Tooltip>
          <Typography className={classes.title}>{name}</Typography>
          <IconButton
            className={classes.viewSource}
            onClick={() => this.setState({ viewSource: true })}
            aria-label="View contract source-code"
          >
            <Tooltip title="View contract source-code">
              <CodeIcon />
            </Tooltip>
          </IconButton>
          <IconButton
            className={classes.viewInEtherscan}
            onClick={() =>
              window.open(`${etherscanURL}/address/${address}`, "_blank")
            }
            aria-label="View in Etherscan"
          >
            <Tooltip title="View in Etherscan">
              <EtherscanIcon />
            </Tooltip>
          </IconButton>
          {/* the source code viewer for this contract */}
          {this.state.viewSource && (
            <SourceCode
              name={name}
              onClose={() => this.setState({ viewSource: false })}
            />
          )}
        </CardContent>
      </Card>
    );
  }
}

Contract.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Contract);
