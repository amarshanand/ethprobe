import React from "react";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Slide from "@material-ui/core/Slide";
import Paper from "@material-ui/core/Paper";
import Close from "@material-ui/icons/Cancel";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import InventoryIcon from "@material-ui/icons/Assignment";
import LedgerIcon from "@material-ui/icons/ChromeReaderMode";
import BuyIcon from "@material-ui/icons/ShoppingCart";
import SaleIcon from "@material-ui/icons/Gavel";
import SoldIcon from "@material-ui/icons/CardGiftcard";
import Icon from "@mdi/react";
import { mdiFactory } from "@mdi/js";
import Store from "../store";
import { observer } from "mobx-react";

/**
 * Contents for the Help file
 */

const styles = {
  root: {
    position: "absolute",
    bottom: 0,
    right: 0,
    width: "560px",
    height: "50%",
    padding: "1rem",
    background: "rgb(229,255,229)",
    boxShadow: "0px 0px 10px 4px rgba(143,143,143,1)",
    borderRadius: "2px !important",
    zIndex: "2",
  },
  container: {
    maxHeight: "100%",
    overflowY: "scroll",
  },
  close: {
    position: "fixed",
    top: 0,
    right: 0,
    color: "red",
  },
  section: {
    marginBottom: "1.5rem",
  },
  h5: {
    margin: "0.75rem 0 0.5rem 0",
  },
  h6: {
    marginBottom: "0.35rem",
  },
  br: {
    opacity: 0,
    marginBottom: "1rem",
  },
  icon: {
    verticalAlign: "top",
    marginRight: "0.25rem",
  },
  code: {
    color: "brown",
    fontSize: "0.8rem",
  },
  seal: {
    width: "2rem",
    marginRight: "0.5rem",
    verticalAlign: "text-bottom",
  },
  videos: {
    width: "560px",
    height: "315px",
  },
};

@observer
class Help extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Slide direction="up" in={Store.showHelp} mountOnEnter unmountOnExit>
        <Paper className={classes.root}>
          <IconButton className={classes.close} onClick={() => (Store.showHelp = false)}>
            <Tooltip title="Close">
              <Close />
            </Tooltip>
          </IconButton>
          <div className={classes.container}>
            <div className={classes.section}>
              <Typography variant="h5" className={classes.h5}>
                <img alt="seal" src="img/seal.png" className={classes.seal} /> EthProbe
              </Typography>
              <div className={classes.br} />
              <div className={classes.section}>
                <iframe
                  className={classes.videos}
                  src="https://www.youtube.com/embed/videoseries?list=PL3FOIyArJIEPZvpNtJ7dJQX-Q_wERngmt"
                  title="YouTube video player"
                  frameborder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowfullscreen
                />
              </div>
              <Typography variant="body2">
                EthProbe simulates a virtual town where citizens can produce and trade anything. Govt of EthProbe has established
                <code className={classes.code}> 𝔽 (Finney)</code> as its currency.
                <div className={classes.br} />
                Note that <code className={classes.code}>1eth = 1000𝔽</code>.<div className={classes.br} />
                The citizens need to login through Metamask and register themselves. They can produce anything for{" "}
                <code className={classes.code}>10𝔽</code> by asking the <code className={classes.code}>Factory</code>. Once produced, all goods get
                advertised on the Marketplace, priced at whatever the seller chooses to.
                <div className={classes.br} />
                Anyone can buy anything if they have the money for it. In the spirit of Ethereum,
                <i> everyone is completely visible to everyone</i>.
              </Typography>
            </div>
            <div className={classes.section}>
              <Typography variant="h6" className={classes.h6}>
                User controls
              </Typography>
              <Typography variant="body2">
                User's account balance, products they have purchased and products they are selling are displayed in the top-right corner.
                <div className={classes.br} />
                <Icon path={mdiFactory} size={1} className={classes.icon} /> would order a production from the Factory for{" "}
                <code className={classes.code}>10𝔽</code>.<div className={classes.br} />
                <BuyIcon className={classes.icon} /> would let the user buy a product from the Marketplace. Only products that the user can afford to
                buy will be shown.
                <div className={classes.br} />
                <InventoryIcon className={classes.icon} /> would display all the products that belong to the user. ⇧ is for the products they are
                selling, and ⇩ is for the products they have purchased.
                <div className={classes.br} />
                <LedgerIcon className={classes.icon} /> would display the financials of the user. -ve values indicate money spent on production or
                purchase, and +ve values indicate money earned by selling a product.
              </Typography>
            </div>
            <div className={classes.section}>
              <Typography variant="h6" className={classes.h6}>
                Marketplace
              </Typography>
              <Typography variant="body2">
                Marketplace lists all the products that have ever been produced.
                <div className={classes.br} />
                <SaleIcon className={classes.icon} /> are products for Sale. It would display the product build-date and its seller.
                <div className={classes.br} />
                <SoldIcon className={classes.icon} /> are products that have been sold. In addition to the above, it would also display the product
                sold-date and its buyer.
              </Typography>
            </div>
            <div className={classes.section}>
              <Typography variant="h6" className={classes.h6}>
                SmartContracts
              </Typography>
              <Typography variant="body2">
                Bottom of the screen presents four SmartContracts. Consider these as the <i>law-book</i> for the town. They decide how everything
                (account registration, production, selling and buying) take place in the EthProbe.
              </Typography>
            </div>
          </div>
        </Paper>
      </Slide>
    );
  }
}

Help.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Help);
