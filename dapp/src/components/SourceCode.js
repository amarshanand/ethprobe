import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Close from '@material-ui/icons/Cancel';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import PropTypes from 'prop-types';
import axios from 'axios';
import Fade from '@material-ui/core/Fade';

import AceEditor from 'react-ace';
import 'brace/ext/beautify';
import 'brace/ext/language_tools';
import 'brace/mode/javascript';
import 'brace/theme/tomorrow_night_bright';

import Store from '../store';
import { Rnd } from 'react-rnd';
import { Typography } from '@material-ui/core';

/**
 * Source Code viewer for Solidity files
 */

const styles = {
    root: {
        position: 'fixed !important',
        background: 'black',
        borderRadius: '3px',
        border: '2px solid rgb(30,30,30)',
        boxShadow: '0px 0px 6px 6px rgb(160,160,160)'
    },
    container: {
        padding: 20
    },
    close: {
        position: 'absolute',
        top: 0,
        right: 0,
        color: 'red',
        zIndex: Store.sourceCodeWindowZindex + 11
    },
    name: {
        position: 'absolute',
        bottom: '0.25rem',
        right: '0.25rem',
        borderRadius: '3px',
        padding: '0.25rem 0.75rem',
        color: 'cyan',
        background: 'rgb(60,60,60)',
        fontSize: '0.8rem',
        letterSpacing: '1px',
        zIndex: Store.sourceCodeWindowZindex + 11
    }
};

class SourceCode extends React.Component {
    state = {
        contents: 'Loading ...',
        zIndex: Store.sourceCodeWindowZindex++,
        minWidth: window.outerWidth / 3,
        width: window.outerWidth / 2.5,
        height: window.outerHeight / 2,
        minHeight: window.outerHeight / 3,
        x: 0,
        y: -1 * window.outerHeight / 2 - Math.random() * 150
    };

    bringToFront = () => this.setState({ zIndex: Store.sourceCodeWindowZindex++ });

    componentDidMount() {
        axios.get(`contracts/${this.props.name}.sol`).then((response) => {
            this.setState({ contents: response.data });
        });
    }

    render() {
        const { onClose, classes, name } = this.props;
        const { width, height, minWidth, minHeight } = this.state;
        return (
            <Rnd
                className={classes.root}
                style={{ zIndex: this.state.zIndex }}
                size={{ width: this.state.width, height: this.state.height }}
                minWidth={minWidth}
                minHeight={minHeight}
                position={{ x: this.state.x, y: this.state.y }}
                onClick={this.bringToFront}
                onDragStart={this.bringToFront}
                onDragStop={(e, d) => {
                    this.setState({ x: d.x, y: d.y });
                }}
                onResize={(e, direction, ref, delta, position) => {
                    this.setState({
                        width: parseInt(ref.style.width),
                        height: parseInt(ref.style.height),
                        ...position
                    });
                }}
            >
                <IconButton className={classes.close} onClick={onClose}>
                    <Tooltip title="Close">
                        <Close />
                    </Tooltip>
                </IconButton>
                <div
                    className={classes.container}
                    style={{
                        width: width - 40,
                        height: height - 60,
                        maxWidth: width - 40,
                        maxHeight: height - 60,
                        overflow: 'scroll'
                    }}
                >
                    {
                        <AceEditor
                            className={classes.ace}
                            readOnly={true}
                            defaultValue={this.state.contents}
                            value={this.state.contents}
                            width={`${width - 40}`}
                            height={`${height - 60}`}
                            mode="javascript"
                            theme="tomorrow_night_bright"
                            name={this.props.name}
                            fontSize={12}
                            showPrintMargin={false}
                            showGutter={false}
                            highlightActiveLine={false}
                            editorProps={{ $blockScrolling: 'Infinity' }}
                            style={{ lineHeight: '1.6em', letterSpacing: '0.75px' }}
                            setOptions={{
                                showLineNumbers: true,
                                displayIndentGuides: false,
                                tabSize: 2,
                                maxLines: Infinity
                            }}
                        />
                    }
                </div>
                <div className={classes.name}>
                    <code>{name}.sol</code>
                </div>
            </Rnd>
        );
    }
}

SourceCode.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SourceCode);
