import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { observer } from 'mobx-react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Table from './Table';
import Tooltip from '@material-ui/core/Tooltip';
import SaleIcon from '@material-ui/icons/Gavel';
import SoldIcon from '@material-ui/icons/CardGiftcard';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import moment from 'moment';

import Store from '../store';

/**
 * A collection of Products form the marketplace. Products can assume {SELLING,SOLD} states.
 * Each Product would have {name, status, seller, price, producedAt, seller, soldAt}
 */

const styles = {
    root: {
        width: '25rem'
    },
    summary: {
        position: 'relative'
    },
    _sold: {
        opacity: 0.6
    },
    status: {
        position: 'absolute',
        top: '0.8rem',
        left: '1.4rem',
        opacity: 0.6
    },
    name: {
        position: 'absolute',
        top: '0.8rem',
        left: '4.55rem',
        fontWeight: 'bold',
        maxWidth:'13rem',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
    },
    price: {
        position: 'absolute',
        top: '0.8rem',
        right: '2rem',
        color: 'rgb(225, 0, 80)',
        fontSize: '0.9rem',
        fontWeight: 'bold',
        letteSpacing: '2px'
    },
    flash: {
        backgroundColor: 'rgba(33,150,243, 0.2)',
        transition: 'background-color 0.1s'
    },
    noFlash: {
        backgroundColor: 'transparent',
        transition: 'background-color 1s'
    }
};

@observer
class Product extends Component {
    state = { expanded: false };
    render() {
        const { classes, pid } = this.props;
        const product = Store.products[pid];
        const forSale = product.status === 'SELLING';
        const seller = Store.users[product.seller] ? Store.users[product.seller] : { name: '-', hash: '' };
        const buyer = Store.users[product.buyer] ? Store.users[product.buyer] : { name: '-', hash: '' };
        return (
            <ExpansionPanel
                className={`${classes.root} ${forSale ? '' : classes._sold} ${Store.flash[pid]
                    ? classes.flash
                    : classes.noFlash}`}
                expanded={this.state.expanded}
                onChange={() => this.setState({ expanded: !this.state.expanded })}
                onMouseEnter={() => {
                    Store.highlightUser = `${seller.hash} ${buyer.hash}`;
                }}
                onMouseLeave={() => {
                    Store.highlightUser = '';
                }}
            >
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} className={classes.summary}>
                    {forSale && (
                        <Tooltip title="For Sale">
                            <SaleIcon className={classes.status} />
                        </Tooltip>
                    )}
                    {!forSale && (
                        <Tooltip title="Sold">
                            <SoldIcon className={classes.status} />
                        </Tooltip>
                    )}
                    <Tooltip title={product.desc}><Typography className={classes.name}>{product.desc}</Typography></Tooltip>
                    <Typography className={classes.price}>{product.price}𝔽</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Table
                        rows={[
                            [ 'Listed on', product.listTime, '', product.listedTransactionHash ],
                            [ 'Seller', seller.name, '', `search://${seller.name}:${seller.hash}` ]
                        ].concat(
                            forSale
                                ? []
                                : [
                                      [ 'Sold on', product.sellTime, '', product.soldTransactionHash ],
                                      [ 'Buyer', buyer.name, '', `search://${buyer.name}:${buyer.hash}` ]
                                  ]
                        )}
                    />
                </ExpansionPanelDetails>
            </ExpansionPanel>
        );
    }
}

Product.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Product);
