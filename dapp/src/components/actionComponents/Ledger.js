import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import { withStyles } from '@material-ui/core/styles';
import Table from '../Table';

import Store from '../../store';

const styles = {
    root: {}
};

/* A table of all SELLING and PURCHASED products */
@observer
class Ledger extends Component {
    closeAction = () => this.props.closeAction('Ledger');

    render() {
        const { show, hash, classes } = this.props;
        return (
            <Collapse in={show} timeout="auto" unmountOnExit>
                <CardContent className={classes.root} disabled={true}>
                    <Table rows={Store.users[hash].ledger} />
                </CardContent>
            </Collapse>
        );
    }
}

Ledger.propTypes = {
    hash: PropTypes.string.isRequired
};

export default withStyles(styles)(Ledger);
