import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { observer } from 'mobx-react';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import DoneIcon from '@material-ui/icons/CheckCircle';
import Tooltip from '@material-ui/core/Tooltip';

import Store from '../../store';
import Blockchain from '../..//blockchain';
import AutoComplete from '../AutoComplete';

const styles = {
    root: {
        marginTop: '-1.5rem',
        position: 'relative'
    },
    name: {
        margin: '0.5rem 1rem 0.5rem 0'
    },
    done: {
        position: 'absolute',
        top: '0.75rem',
        right: '0.65rem',
        color: 'green'
    }
};

/* A mini form for producing products */
@observer
class Buy extends Component {
    state = { value: null };
    onChange = (value) => {
        this.setState({ value });
    };

    closeAction = () => {
        this.props.closeAction('buy');
        Blockchain.doBuy({ pid: this.state.value.value, buyer: this.props.hash });
    };

    render() {
        const { show, classes } = this.props;
        const { value } = this.state;
        return (
            <Collapse in={show} timeout="auto" unmountOnExit>
                <CardContent className={classes.root} disabled={true}>
                    <AutoComplete
                        placeholder="Product Name"
                        options={Store.buyableProductsFor(this.props.hash)}
                        value={this.state.value}
                        className={classes.name}
                        onChange={this.onChange}
                    />
                    <IconButton
                        disabled={!value}
                        className={classes.done}
                        onClick={this.closeAction}
                        aria-label="Commit"
                    >
                        <Tooltip title="Commit this Buy">
                            <DoneIcon />
                        </Tooltip>
                    </IconButton>
                </CardContent>
            </Collapse>
        );
    }
}

Buy.propTypes = {
    hash: PropTypes.string.isRequired
};

export default withStyles(styles)(Buy);
