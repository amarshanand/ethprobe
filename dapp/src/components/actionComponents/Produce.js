import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { observer } from 'mobx-react';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import DoneIcon from '@material-ui/icons/CheckCircle';
import Tooltip from '@material-ui/core/Tooltip';

import Blockchain from '../..//blockchain';

const styles = {
    root: {
        marginTop: '-1.5rem',
        position: 'relative'
    },
    name: {
        margin: '0.5rem 2rem 0.5rem 0',
        fontSize: '0.8rem',
        width: 'calc(100% - 11rem)'
    },
    price: {
        marginTop: '0.5rem',
        fontSize: '0.8rem',
        width: '4rem'
    },
    dollar: {
        display: 'inline-block',
        fontSize: '0.8rem',
        opacity: 0.8,
        margin: '0.95rem 0.4rem 0 0.5rem'
    },
    done: {
        position: 'absolute',
        bottom: '1.35rem',
        right: '0.65rem',
        color: 'green'
    }
};

/* A mini form for producing products */
@observer
class Produce extends Component {
    state = { name: '', price: '' };

    handleChange = (name) => (event) => {
        this.setState({ [name]: event.target.value });
    };

    closeAction = () => {
        this.props.closeAction('produce');
        Blockchain.doProduce(this.state.name, parseInt(this.state.price));
    };

    render() {
        const { show, classes } = this.props;
        const { name, price } = this.state;
        return (
            <Collapse in={show} timeout="auto" unmountOnExit>
                <CardContent className={classes.root} disabled={true}>
                    <TextField
                        placeholder="Product Description"
                        className={classes.name}
                        value={this.state.name}
                        onChange={this.handleChange('name')}
                    />
                    <div className={classes.dollar}>$</div>
                    <TextField
                        type="number"
                        placeholder="Price"
                        className={classes.price}
                        value={this.state.price}
                        InputProps={{ inputProps: { min: 0 } }}
                        onChange={this.handleChange('price')}
                    />
                    <IconButton
                        disabled={name.trim() === '' || price <= 0}
                        className={classes.done}
                        onClick={this.closeAction}
                        aria-label="Commit"
                    >
                        <Tooltip title="Commit this produce">
                            <DoneIcon />
                        </Tooltip>
                    </IconButton>
                </CardContent>
            </Collapse>
        );
    }
}

Produce.propTypes = {
    hash: PropTypes.string.isRequired
};

export default withStyles(styles)(Produce);
