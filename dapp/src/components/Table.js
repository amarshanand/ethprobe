import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import { etherscanURL } from '../config';
import Store from '../store';

const styles = (theme) => ({
    root: {
        marginTop: '-1rem',
        border: '1px solid rgba(0,0,0,0.1)',
        width: '100%',
        maxHeight: '12rem',
        overflowY: 'scroll'
    }
});

function SimpleTable(props) {
    const { classes } = props;
    return (
        <Paper className={classes.root} elevation={0}>
            <Table className={classes.table}>
                {props.header && (
                    <TableHead>
                        <TableRow key={Math.random()}>
                            {props.header.map((col, i) => <TableCell key={i}>{col}</TableCell>)}
                        </TableRow>
                    </TableHead>
                )}
                <TableBody>
                    {props.rows.map((row, i) => {
                        // if the row has 4 cols (say, for ledger), last element is the the tx hash or a search param
                        return (
                            <TableRow
                                className={row.length >= 4 ? 'transaction' : ''}
                                key={i}
                                onClick={() => {
                                    if (row.length >= 4) {
                                        if (row[3].startsWith('search://'))
                                            Store.searchKW = row[3].substring('search://'.length).split(':')[0];
                                        else window.open(`${etherscanURL}/tx/${row[3]}`, '_blank');
                                    }
                                }}
                                onMouseEnter={() => {
                                    row[3] &&
                                        row[3].startsWith('search://') &&
                                        (Store.highlightUser = row[3].split(':').pop());
                                }}
                                onMouseLeave={() => (Store.highlightUser = null)}
                            >
                                {row
                                    .slice(0, 3)
                                    .map((col, j) => (
                                        <TableCell
                                            key={j}
                                            padding={'dense'}
                                            dangerouslySetInnerHTML={{ __html: col }}
                                        />
                                    ))}
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </Paper>
    );
}

SimpleTable.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SimpleTable);
