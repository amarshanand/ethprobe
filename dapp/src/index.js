require('dotenv').config();

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Store from './store';
import Blockchain from './/blockchain';

Store.init();
Blockchain.init();

// check some vital preconditions before launching the app
if (!process.env.REACT_APP_ETHERSCAN_API_KEY)
    Store.crashMessage = 'Either .env file is not present , or it doesnt have an entry for REACT_APP_ETHERSCAN_API_KEY';

ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.unregister();
