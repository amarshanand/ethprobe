import { observable, action } from 'mobx';
import { users } from './users';
import Blockchain from '..//blockchain';
import { waitBeforeAskingForSignup } from '../config';

/**
 *	Global state that deals with the entire App. SmartContract for market events (production, selling) would trigger an event,
 *  which would be used to alter the state of the App here.
 */
class Store {
    /* a fatal maessage, which immediately creashes the App */
    @observable crashMessage = null;

    /* this flag will be set to true when we want to invite the user for a signup */
    @observable showSignup = false;

    /* show / hide the discussion forum */
    @observable showDisqus = false;

    /* show / hide Help */
    @observable showHelp = false;

    /* currently loggedIn user. 0x0 stands for Admin */
    @observable loggedInUser = '0x0';

    /* search keywords (to filter the users) */
    @observable searchKW = '';

    /* list of users - by reading ./seed.js */
    @observable users = {};

    /* an inventory of all good in the marketplace */
    @observable products = [];

    /* list of all deployed SmartContracts */
    @observable contracts = {};

    /* issue app-wide notifications */
    @observable notification = null;

    /* flash the given user */
    @observable flash = {};

    /* highlight the given user, typically while hovering over it in the marketplace */
    @observable highlightUser = {};

    /* flag that the accounts and marketplace are loading */
    @observable accountsLoading = true;
    @observable marketplaceLoading = true;

    /* the zIndex of the source-code window */
    sourceCodeWindowZindex = 111;

    constructor() {}

    @action
    init() {
        // decorate the users obtained from config with more properties
        Object.entries(users).forEach(([ hash, user ]) => this.createUser(hash, user));
        // ensure that we have a Factory and an Admin at the least from users.js
        if (!this.factoryHash || !this.adminHash)
            return (this.crashMessage = "The file user.js must contain a user named 'Factory` and one named `Admin`");
    }

    /**
     * Starting from a User definition that is obtained from user.js, we create a User that we use throught the app
     * @param {} user 
     */
    createUser(hash, user) {
        hash = hash.toLowerCase();
        this.massageUser(user);
        this.users[hash] = {
            hash,
            balance: 0,
            storage: 0,
            bought: 0,
            asset: 0,
            locked: false,
            products: {},
            ledger: [],
            inventory: [],
            pendingTxn: {},
            stats: { assetCount: 0, assetValue: 0, purchaseCount: 0, purchaseValue: 0 },
            ...user
        };
        Blockchain.getBalanceForAddress(hash, (balance) => (this.users[hash].balance = balance));
        user.name === 'Factory' && (this.factoryHash = hash);
        user.name === 'Admin' && (this.adminHash = hash);
        return user;
    }

    /* array of <name, address> for all deployed contracts */
    get contractList() {
        const contractList = [];
        Object.entries(this.contracts).forEach(([ name, { address } ]) => contractList.push({ name, address }));
        return contractList;
    }

    /* login the user and notify the UI. called by Blockchain each time the user changed in Metamask */
    @action
    loginUser(_loggedInUser) {
        this.notification = null;
        this.loggedInUser = _loggedInUser;
        // if the user is loggedIn to Metamask but his account is not found, we invite them to signup
        if (!this.users[_loggedInUser]) {
            return setTimeout(() => (this.showSignup = !this.users[_loggedInUser]), waitBeforeAskingForSignup);
        }
        this.showSignup = false;
        this.doNotify(`${this.users[_loggedInUser].name} is Logged In`);
        this.doFlash(_loggedInUser);
    }

    /**
     * Add the fees to the given transaction. It is a separate method since we add the fees after the txn is added to the ledger
     * @param {*} hash 
     * @param {*} txnHash 
     * @param {*} fees 
     */
    @action
    addTxnFees(hash, txnHash, fees) {
        const { ledger } = this.users[hash];
        const newLedger = [];
        for (let i = 0; i < ledger.length; i++) {
            ledger[i][3] === txnHash && (ledger[i][2] += `<span class='gas'>- ${fees}</span>`);
            newLedger.push(ledger[i]);
        }
        this.users[hash].ledger = newLedger;
    }

    /**
     * lock the user or contract with given hash during a transaction. this is to avoid double-spending situations.
     * @param hash address of the user / contract
     * @param isContract if true, we assume that we are locking a contract and not a user
     **/
    @action
    doLock(hash, isContract) {
        isContract && this.contracts[hash] && (this.contracts[hash].locked = true);
        !isContract && this.users[hash] && (this.users[hash].locked = true);
    }

    /* lock the user or contract with given hash during a transaction. this is to avoid double-spending situations */
    @action
    doUnlock(hash, isContract) {
        isContract && this.contracts[hash] && (this.contracts[hash].locked = false);
        !isContract && this.users[hash] && (this.users[hash].locked = false);
    }

    /* flash the user or product with the given hash */
    @action
    doFlash(id) {
        this.flash[id] = true;
        setTimeout(() => delete this.flash[id], 2000);
    }

    /* notify the UI (app.js opens a snackbar to consume it). possible types are {error, info, warning} */
    @action
    doNotify(text, type = 'info', onClick) {
        this.notification = { type, text, onClick };
    }

    /* array of all products in the market*/
    get productList() {
        const productList = [];
        Object.entries(this.products).forEach(([ pid, product ]) => productList.unshift({ pid, product }));
        return productList.sort((a, b) => b.product.listTime - a.product.listTime);
    }

    /**
     * list of {pid, name} for products that can be bought by the user with the given hash (ie, price <= user's balance)
     * @param {*} hash 
     */
    buyableProductsFor(hash) {
        if (hash != this.loggedInUser) return [];
        const { balance } = this.users[hash];
        const productList = [];
        Object.entries(this.products).forEach(([ pid, product ]) => {
            if (product.status === 'SELLING' && product.price <= balance && product.seller !== hash)
                productList.push({ value: pid, label: product.desc });
        });
        return productList;
    }

    /**
     * For a user that has just been retrieved from /store/users.js, ensure that all fields are present. If some are missing,
     * we randomly allocate values to them, so that our UI looks good and clean
     * @param {} user 
     */
    massageUser(user) {
        const firstnames = [ 'Bill', 'Thomas', 'Alan', 'Bob', 'Ram', 'Vinod', 'Satya', 'Lee', 'Jack', 'Jackie' ];
        const lastnames = [ 'Gates', 'Edison', 'Turing', 'Dylan', 'Charan', 'Dham', 'Nadella', 'KaiFu', 'Ma', 'Chan' ];
        if (!user.name) user.name = `${firstnames[~~(Math.random() * 10)]} ${lastnames[~~(Math.random() * 10)]}`;
        const about = [ 'Focus', 'Belief', 'Honesty', 'Truth', 'Peace', 'Belief', 'Hope', 'Faith', 'Love', 'Trust' ];
        if (!user.about) user.about = about[~~(Math.random() * 10)];
        if (!user.image) user.image = `img/avatars/${~~(Math.random() * 10)}.png`;
        if (!user.description) user.description = ``;
    }

    /**
     * Copy the given string to clipboard and issue a notification
     * ref: https://stackoverflow.com/questions/127040/copy-put-text-on-the-clipboard-with-firefox-safari-and-chrome
     * @param {*} string
     */
    copyToClipboard(string) {
        const handler = (event) => {
            event.clipboardData.setData('text/plain', string);
            event.preventDefault();
            document.removeEventListener('copy', handler, true);
            this.doNotify(`Copied to clipboard : ${string}`);
        };
        document.addEventListener('copy', handler, true);
        document.execCommand('copy');
    }
}

export default new Store();
