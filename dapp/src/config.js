module.exports = {
  fakeNameGenerator: "https://randomuser.me/api/",
  costOfProduction: 10,
  waitBeforeAskingForSignup: 3000,
  minBalance: 10,
  etherscanURL: "https://goerli.etherscan.io",
  etherscanAPI: `https://api-goerli.etherscan.io/api?module=account&action=txlist&apikey=${
    process.env.REACT_APP_ETHERSCAN_API_KEY
  }&address=`,
  projectRepo: "https://gitlab.com/amarshanand/ethprobe",
  socialSharing: {
    shareUrl: String(window.location),
    title: "EthProbe - Probing Ethereum through a Marketplace App",
    hashtag: "#ethprobe",
    description:
      "EthProbe is a simple Marketplace WebApp, similar to eBay. \
             Unlike eBay, which stores all the data and transactions in-house, EthProbe uses Ethereum blockchain for its storage. \
             The App comes with a three stage tutorial - Layman, Tech-curious and Programmer. \
             Its purpose is to demistify what is Ethereum and what does it promises to deliver. \
             EthProbe does not talk about Cryptocurrency or the internal workings of Ethereum",
  },
  disqus: {
    shortname: "ethprobe",
    url: "https://ethprobe.com",
    identifier: "ethprobe",
    title: "EthProbe - Probing Ethereum through a Marketplace App",
  },
};
