import "@metamask/legacy-web3";
import Store from "../store";
import { initAdmin } from "./admin";
import { initFactory } from "./factory";
import { initMarketplace } from "./marketplace";
import { initAccounts } from "./accounts";
import { initUser } from "./user";
import { minBalance } from "../config";

/**
 * This service is our proxy for Ethereum SmartContrcats. Its design allows it to interact with any async service
 * (Hyperledger, Node Server, etc) that exposes rest endpoints, and triggers events.
 */
class Blockchain {
  /* events are being fired multiple times, so we need to cache the txnHash to avoid duplicates */
  pastEvents = {};

  /* currently, we are supporting Goerli (chain_id = "0x5", network_id = 5) and Matic (chain_id = "0x13881", network_id = 80001) */
  network;

  /**
   * In the constructor, we create our SmartContrcat instances, which we can use later to execute code on the blockchain
   */
  constructor() {
    if (window.web3) this.web3 = new Web3(web3.currentProvider);
  }

  /**
   * To be called at starting of the App. This method reads all the events since the beginning of time, and sets the state
   * in the Store. All further events are applied to the Store to alter the state of the system incrementally.
   */
  async init() {
    if ((await this.validateMetamask()) === false) return null;
    // running 'npm deploy' in /blockchain folder will deploy our contracts and will put the ABIs in /blockchain/build/contracts
    // /src/contracts is a symlink that would help up access these ABIs
    const setContract = (contract) => {
      const {
        contractName,
        abi,
        networks,
      } = require(`./contracts/${contract}`);
      const { address } = networks[this.network === "0x5" ? "5" : "80001"];
      this[contractName] = web3.eth.contract(abi).at(address);
      Store.contracts[contractName] = { address };
    };
    ["Accounts", "Factory", "Marketplace", "User"].forEach(setContract);
    if (!Store.factoryHash || !Store.adminHash)
      return (Store.crashMessage =
        "Admin and Factory must be present in store/users.js");
    // set the Admin ledger from Etherscan. Other Users Emit Events, but Admin only deploys the Contracts to the blockchain
    initAdmin();
    // init all contracts (ie, process all past Events, set handlers for all future events)
    initFactory(this.Factory);
    initMarketplace(this.Marketplace);
    // while Factory and Marketplace are independednt of the accounts, to init User, we first need to init Accounts
    initAccounts(this.Accounts, () => {
      // Metamask would seed web3 injection with the account the user is loggedIn with. we will use this to login our user
      this.setUserFromMetamask({ selectedAddress: web3.eth.accounts[0] });
      window.ethereum.on("accountsChanged", (e) => {
        this.setUserFromMetamask({ selectedAddress: e[0] });
      });
      // once all accounts are set, we init all Users
      initUser(this.User);
    });
  }

  /**
   * Validate a bunch of things about Metamask, including whether it is installed or not, and whether Goerli Test Network is selected.
   * It returns false is web3 is missing, and then ensures that network is set to Goerli (value '3'). There are situations when web3.version.network
   * comes out to be null, in which case, we keep on reading it until we get a non-null value
   */
  async validateMetamask() {
    // if web3 is not detected, the user hasnt perhaps installed Metamask
    if (
      window.ethereum == null ||
      !window.ethereum ||
      typeof window.ethereum == "undefined" ||
      !window.ethereum.isMetaMask
    ) {
      Store.crashMessage =
        'Metamask is not installed or is disabled. Please visit the page after installing Metamask.\
                <br/><br/><a href="https://en.bitcoinwiki.org/wiki/Metamask" target="_blank" class="whitelink">Click here to read more about Metamask and why is it needed</a>\
                <br/><br/><a href="https://metamask.io/" target="_blank" class="whitelink">Click here to install Metamask</a>';
      return false;
    }
    if (window.ethereum)
      await window.ethereum.request({ method: "eth_requestAccounts" });
    window.web3 = new Web3(window.ethereum);
    // if the selected network is not Goerli (network 0x3), we cant proceed
    this.network = await window.ethereum.request({ method: "eth_chainId" });
    if (!this.network)
      return setTimeout(async () => await this.validateMetamask(), 0);
    if (!["0x5", "0x13881"].includes(this.network)) {
      Store.crashMessage =
        "EthProbe works only with Goerli or Matic Test Network. You have chosen a diferent network in Metamask.<br/><br/>Please switch to Goerli or Matic Test Network in Metamask to use EthProbe.";
      return false;
    }
  }

  /**
   * set the user for the App. This happens at the start, and then upon everytime the user changes in Metamask
   */
  setUserFromMetamask({ selectedAddress }) {
    // we need to perform a delayed execution since multiple events are being emitted, and we only want to select the last one
    const setUser = () => {
      // first ensure that the user is loggedIn to Metamask. If this isnt the case, then selectedAddress is null
      if (!this.selectedAddress) {
        Store.showSignup = false;
        setTimeout(
          () =>
            this.setUserFromMetamask({ selectedAddress: web3.eth.accounts[0] }),
          5000
        );
        return Store.doNotify(
          `Login failed. Are you logged-in to Metamask?`,
          "error"
        );
      }
      this.web3.eth.defaultAccount = this.selectedAddress.toLowerCase();
      if (Store.loggedInUser == this.selectedAddress) return;
      Store.loginUser(this.selectedAddress);
    };
    // we just remember the selectedAddress, and defer execution. This way, only the last value of selectedAddress is used
    this.selectedAddress = selectedAddress;
    setTimeout(setUser, this.selectedAddress ? 0 : 1000);
  }

  /**
   * Given the details, exceute the Accounts SmartContract to register the currently logged-in user to EBMS
   */
  doSignup({ name, about, description, image, url }) {
    // first we create a user and add it to the list of users, so this it looks like any other user
    const hash = Store.loggedInUser;
    const addLogedInUser = () => {
      Store.createUser(hash, { name, about, description, image, url });
      this.getBalanceForAddress(
        hash,
        (balance) => (Store.users[hash].balance = balance)
      );
    };
    const removeLoggedInUser = () => delete Store.users[hash];
    addLogedInUser();
    Store.doLock(hash);
    // then we execute the Accounts.addAccount() SmartContract for this user
    this.doTransact(
      this.Accounts.addAccount,
      [name, about, description, image, url],
      0,
      `registering ${name}`,
      "Accounts",
      (err) => err && removeLoggedInUser(),
      (err) => err && removeLoggedInUser()
    );
  }

  /**
   * Get the balance in finney for the given wallet address.
   * 1 eth = 1000 finney
   * @param {*} address
   */
  async getBalanceForAddress(address, done) {
    if (address == 0x0 || (await this.validateMetamask()) === false) return;
    this.web3.eth.getBalance(address, (err, balance) => {
      if (!balance) return -1;
      done(parseInt(web3.fromWei(balance.toString(), "finney")));
      // issue a low balance warning
      Store.loggedInUser !== Store.factoryHash &&
        Store.users[Store.loggedInUser] &&
        balance <= minBalance &&
        Store.doNotify(
          "Low balance. Click here to get Ethers from https://goerlifaucet.com",
          "error",
          () => window.open(`https://goerlifaucet.com`, "_blank")
        );
    });
  }

  /**
   * This method performs production of new products. It calls the User.createProduct() method, and waits for the UserLedgerUpdated
   * and UserInventoryUpdated events to be fired. Cost of production is 10 Finney = 10000000000000000 wei.
   * @param {*} desc
   * @param {*} price in Finney
   */
  doProduce(desc, price) {
    this.doTransact(
      this.User.createProduct,
      [price, desc],
      10,
      `Producing ${desc}`
    );
  }

  /**
   * This method performs production of new products. It calls the User.buyProduct() method, and waits for the UserLedgerUpdated
   * and UserInventoryUpdated events to be fired. The price amount of Finney must be passed for the Sale to occur.
   * @param {*} pid
   */
  doBuy({ pid }) {
    const { price, desc } = Store.products[pid];
    this.doTransact(this.User.buyProduct, [pid], price, `Buying ${desc}`);
  }

  /**
   * A generic method that executes a txnFunction(). Its does neccessary updates to the UI (locking, notification, etc)
   * @param {*} txnFunction
   * @param {*} args
   * @param {*} value
   * @param {*} caption
   * @param {*} contract which SmartContract to hit. Defaults to User
   * @param {*} submitted a callback when the txn is submitted to the SmartContract
   * @param {*} done a callback when the txn has been mined
   */
  doTransact(
    txnFunction,
    args,
    value,
    caption,
    contract = "User",
    sumbitted,
    done
  ) {
    // to check whether the txn suuceedd or not, we need to poll. this is better for web3 1.x, but we are on 0.2.x
    const pollTxn = (hash, done) => {
      web3.eth.getTransactionReceipt(hash, (err, receipt) => {
        if (receipt) return done(!err && parseInt(receipt.status) === 1);
        setTimeout(() => pollTxn(hash, done), 1000);
      });
    };
    const hash = Store.loggedInUser;
    const user = Store.users[hash];
    if (contract === "User") Store.doLock(hash);
    Store.doFlash(Store.contracts[contract].address);
    const callback = (err, txnHash) => {
      if (contract === "User") Store.doUnlock(hash);
      if (err) {
        Store.doNotify(`Error while submitting transaction : ${err}`, "error");
        return sumbitted && sumbitted(err);
      }
      user.pendingTxn[txnHash] = { caption, counter: 0 };
      this.getBalanceForAddress(
        user.hash,
        (balance) => (user.balance = balance)
      );
      Store.doNotify(`Transaction submitted for ${caption}`);
      sumbitted && sumbitted();
      pollTxn(txnHash, (didSucceed) => {
        if (didSucceed) return done && done();
        Store.doNotify(`Transaction for ${caption} failed`, "error");
        delete user.pendingTxn[txnHash];
        done && done(new Error(`Transaction for ${caption} failed`));
      });
    };
    txnFunction.apply(null, [
      ...args,
      { value: web3.toWei(value, "finney") },
      callback,
    ]);
  }
}

// export a singleton
export default new Blockchain();
