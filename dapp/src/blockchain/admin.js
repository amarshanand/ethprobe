import axios from 'axios';
import { etherscanAPI } from '../config';
import moment from 'moment';
import Store from '../store';

/**
 * All functionalities pertaining to the Admin User. As opposed to other Users, Admin User (used for uploading SmartContracts)
 * does not emit events. Hence, we have to use Etherscan API to get the Admin ledger.
 */

// Set ledger for Admin by calling Etherscan API
export function initAdmin() {
    Store.doLock(Store.adminHash);
    const admin = Store.users[Store.adminHash];
    axios
        .get(`${etherscanAPI}${admin.hash}`)
        .then((response) => {
            admin.ledger = response.data.result
                .reverse()
                .map(({ hash, timeStamp, gasUsed, gasPrice, contractAddress }) => {
                    const contract = contractAddress && Store.contractList.find((c) => c.address === contractAddress);
                    return [
                        moment.unix(parseInt(timeStamp)).format('DD-MM-YY'),
                        contract ? contract.name : '-',
                        `- ${Math.ceil(web3.fromWei(gasUsed * gasPrice, 'finney'))} 𝔽`,
                        hash
                    ];
                });
            Store.doUnlock(Store.adminHash);
        })
        .catch((error) => {
            Store.doNotify('Could not obtain Ledger for Admin', 'error');
            Store.doUnlock(Store.adminHash);
        });
}
